import pyttsx3                       # transforme mot en voix
import speech_recognition as sr
import os

engine = pyttsx3.init('sapi5')
voices = engine.getProperty('voices')[0]
engine.setProperty('voice', voices.id)

def speak(audio):
    engine.say(audio)
    engine.runAndWait()
 
def takeCommand():
     
    r = sr.Recognizer()

    with sr.Microphone() as source:
         
        print("Listening...")
        r.pause_threshold = 1
        audio = r.listen(source)
  
    try:
        print("Recognizing...")   
        query = r.recognize_google(audio, language ='fr')
        print(f"User said: {query}\n")
  
    except Exception as e:
        print(e)   
        return "None"
     
    return query

if __name__ == '__main__':

    clear = lambda: os.system('cls')
    clear()
     
    while True:

        query = takeCommand().lower()

        if "ok maman" in query and "bonjour" in query:
            speak("bonjour")

        elif "ok maman" in query and "ça va" in query:
            speak("Plutôt bien")
 
